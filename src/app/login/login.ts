import {Component} from '../common/Component';
import * as firebase from 'firebase';
import {inject} from 'aurelia-framework';
import {AppRouter} from 'aurelia-router';

@inject(AppRouter)
export class LoginController extends Component {

  private auth : firebase.auth.Auth;
  private email: string = '';
  private password: string = '';

  
  constructor(private appRouter: AppRouter) {
    super('LoginController');

    this.auth = firebase.auth();
  }

  public logIn() {
    return this.auth.signInWithEmailAndPassword(this.email, this.password).then((firebaseUser: any) => {
      console.log('User ' + firebaseUser.uid + ' logged in successfully!')
      return this.appRouter.loadUrl('/profile');
    }).catch(this.handleError);
  }
}
