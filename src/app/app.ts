import { Aurelia } from 'aurelia-framework';
import { Router, AppRouter, RouterConfiguration, PipelineStep, NavigationInstruction, Next, Redirect } from 'aurelia-router';
import { AuthService } from './common/AuthService';
import { inject } from 'aurelia-framework';

@inject(AppRouter, AuthService)
export class App {
  private router: Router;

  constructor(private appRouter: AppRouter, private authService: AuthService) { }

  configureRouter(config: RouterConfiguration, router: Router) {
    // config.addAuthorizeStep(new AuthorizationStep(this.appRouter, this.authService));
    config.title = 'ArtApp';
    config.map([
      { route: ['', 'home'], name: 'home', moduleId: './home/home', nav: true, title: 'Home' },
      { route: 'login', name: 'login', moduleId: './login/login', nav: true, title: 'Login', settings: { hideFromNav: true } },
      { route: 'register', name: 'register', moduleId: './register/register', nav: true, title: 'Register', settings: { hideFromNav: true } },
      { route: 'profile', name: 'profile', moduleId: './profile/profile', nav: true, title: 'Profile', settings: { auth: true } },
      { route: 'admin', name: 'admin', moduleId: './admin/admin', nav: true, title: 'Admin', settings: { auth: true, admin: true } },
    ]);
    this.router = router;
  }
}

class AuthorizationStep implements PipelineStep {
  constructor(private appRouter: AppRouter, private authService: AuthService) { }

  run(instruction: NavigationInstruction, next: Next) {
    console.log("AppRouter", this.appRouter);
    
    if (instruction && instruction.config.settings.auth) {
      console.log("Trying to enter an authorized page: ", instruction.fragment);
      return this.authService.getUser().then(user => {
        return next();
      }, reason => {
        return next.cancel(new Redirect('login'));
      });
    } else {
      return next();
    }
  }
}