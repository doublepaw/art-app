import { Component } from '../common/Component';
import * as firebase from 'firebase';
import { HttpClient, json } from 'aurelia-fetch-client';
import { inject } from 'aurelia-framework';
import { AuthService } from '../common/AuthService';
import { ChallengeService, ChallengeServiceEvent } from '../common/ChallengeService';
import { TaskPushService } from '../common/TaskPushService';
import { Challenge, ChallengeSuggestion, KeyValue } from 'art-app-common';
import * as _ from 'lodash';

@inject(HttpClient, AuthService, ChallengeService, TaskPushService)
export class ProfileController extends Component {

  private selectedFiles: { [key: string]: File[] } = {};
  private storageReference: firebase.storage.Reference;
  private databaseReference: firebase.database.Reference;
  private imageRefs: any[];
  private user: firebase.User;
  private activeChallenges: KeyValue<Challenge>[];
  private completedChallenges: KeyValue<Challenge>[];
  private suggestedChallenges: KeyValue<ChallengeSuggestion>[];

  constructor(private httpClient: HttpClient, private authService: AuthService, private challengeService: ChallengeService, private taskPushService: TaskPushService) {
    super('ProfileController');

    this.storageReference = firebase.storage().ref();
    this.databaseReference = firebase.database().ref();
    this.user = null;
    this.imageRefs = [];
    this.activeChallenges = [];
    this.completedChallenges = [];
    this.suggestedChallenges = [];

    this.authService.getUser().then(user => {
      this.user = user;
    });

    this.challengeService.setActiveChallengesCallback(ChallengeServiceEvent.Added, this.onActiveChallengeAdded);
    this.challengeService.setActiveChallengesCallback(ChallengeServiceEvent.Removed, this.onActiveChallengeRemoved);

    this.challengeService.setSuggestedChallengesCallback(ChallengeServiceEvent.Added, this.onSuggestedChallengeAdded);
    this.challengeService.setSuggestedChallengesCallback(ChallengeServiceEvent.Removed, this.onSuggestedChallengeRemoved);

    this.challengeService.setCompletedChallengesCallback(ChallengeServiceEvent.Added, this.onCompletedChallengeAdded);
    this.challengeService.setCompletedChallengesCallback(ChallengeServiceEvent.Removed, this.onCompletedChallengeRemoved);
  }

  public getNewSuggestion = () => {
    this.authService.getUser().then(user => {
      var suggestChallengeTask = {
        _state: 'suggest_challenge_start',
        userId: user.uid
      };

      return this.taskPushService.pushTask(suggestChallengeTask);
    }).catch(this.handleError);
  };

  public acceptSuggestion = (challengeSuggestionId: string) => {
    this.authService.getUser().then(user => {
      var acceptChallengeTask = {
        _state: 'accept_suggestion_start',
        userId: user.uid,
        challengeSuggestionId: challengeSuggestionId
      };

      return this.taskPushService.pushTask(acceptChallengeTask);
    }).catch(this.handleError);
  };

  public completeChallenge = (activeChallengeId: string) => {
    this.authService.getUser().then(user => {
      var completeChallengeTask = {
        _state: 'complete_challenge_start',
        userId: user.uid,
        activeChallengeId: activeChallengeId
      };

      return this.taskPushService.pushTask(completeChallengeTask);
    }).catch(this.handleError);
  };



  private onActiveChallengeAdded = (addedChallenge: KeyValue<Challenge>) => {
    this.activeChallenges.push(addedChallenge);
  };

  private onActiveChallengeRemoved = (removedChallenge: KeyValue<Challenge>) => {
    var removedChallengeIndex = this.activeChallenges.findIndex(activeChallenge => activeChallenge.key === removedChallenge.key);
    this.activeChallenges.splice(removedChallengeIndex, 1);
  }
  private onCompletedChallengeAdded = (challenge: KeyValue<Challenge>) => {
    this.completedChallenges.push(challenge);
  };

  private onCompletedChallengeRemoved = (removedChallenge: KeyValue<Challenge>) => {
    var removedChallengeIndex = this.completedChallenges.findIndex(completedChallenge => completedChallenge.key === removedChallenge.key);
    this.completedChallenges.splice(removedChallengeIndex, 1);
  };

  private onSuggestedChallengeAdded = (addedSuggestion: KeyValue<ChallengeSuggestion>) => {
    this.suggestedChallenges.push(addedSuggestion);
  };

  private onSuggestedChallengeRemoved = (removedSuggestion: KeyValue<ChallengeSuggestion>) => {
    var removedItemIndex = this.suggestedChallenges.findIndex(suggestion => suggestion.key === removedSuggestion.key);
    this.suggestedChallenges.splice(removedItemIndex, 1);
  };




}
export class BlobToUrlValueConverter {
  toView(blob) {
    if (!blob) return "";
    return URL.createObjectURL(blob);
  }
}

