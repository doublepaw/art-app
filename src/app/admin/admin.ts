import { Router, RouterConfiguration } from 'aurelia-router';
import { inject } from 'aurelia-framework';

// @inject(Router, RouterConfiguration)
export class Admin {
  public router: Router;
  public title: string;

  constructor() { }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.addAuthorizeStep
    config.title = 'Admin';
    config.map([
      { route: ['challenge-editor', ''], name: 'challengeEditor', moduleId: './challengeEditor/challengeEditor', nav: true, title: 'Challenge Editor' },
      { route: ['tag-editor'], name: 'tagEditor', moduleId: './tagEditor/tagEditor', nav: true, title: 'Tag Editor' }
    ]);
    this.router = router;
  }

}