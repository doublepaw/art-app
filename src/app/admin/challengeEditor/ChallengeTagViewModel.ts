export class ChallengeTagViewModel {
  public id: string;
  public name: string;
  public type: number;
  public selected: boolean;
}