import { Component } from '../../common/Component';
import { ChallengeTagViewModel } from './ChallengeTagViewModel';
import { ChallengeTemplateViewModel } from './ChallengeTemplateViewModel';
import { inject } from 'aurelia-framework';
import { BindingSignaler } from 'aurelia-templating-resources';
import { ChallengeService } from 'common/ChallengeService';
import { ChallengeTagService } from 'common/ChallengeTagService';
import { ChallengeTemplateService } from 'common/ChallengeTemplateService';
import { EnumExtensions } from 'helpers/EnumExtensions';
import { ChallengeTemplate, ChallengeTemplateType, ChallengeTemplateOwner, ChallengeTemplateOwnerType } from 'art-app-common';

@inject(ChallengeTemplateService, ChallengeTagService, BindingSignaler, ChallengeService)
export class ChallengeEditor extends Component {

  public challengeTemplateViewModels: ChallengeTemplateViewModel[];
  public challengeTagViewModels: ChallengeTagViewModel[];
  public templateTypes: { name: string, value: number }[];

  constructor(

    private challengeTemplateService: ChallengeTemplateService,
    private challengeTagService: ChallengeTagService,
    private signaler: BindingSignaler,
    private challengeService: ChallengeService) {
    super('ChallengeEditor');
    this.challengeTemplateViewModels = [];
    this.templateTypes = EnumExtensions.getNamesAndValues(ChallengeTemplateType);
    this.challengeTagViewModels = [];

    this.getChallengeTags().then(() => {
      this.getChallengeTemplates();
    });
  }

  public getChallengeTags = () => {
    return this.challengeTagService.getAll().then(challengeTags => {
      this.challengeTagViewModels = challengeTags.map(challengeTag => {
        var viewModel = new ChallengeTagViewModel();
        viewModel.id = challengeTag.key;
        viewModel.name = challengeTag.value.name;
        viewModel.type = challengeTag.value.type;

        return viewModel;
      });
    }).catch(this.handleError);
  };

  public assignTag = (challengeTemplate: ChallengeTemplateViewModel, challengeTag: ChallengeTagViewModel) => {
    var tagIndex = challengeTemplate.challengeTags.findIndex(ct => ct.id === challengeTag.id);

    if (tagIndex < 0) {
      challengeTemplate.challengeTags.push(challengeTag);
    }
    else {
      challengeTemplate.challengeTags.splice(tagIndex, 1);
    }

    challengeTemplate.hasChanges = true;
    this.signaler.signal('tags-clicked');
  };

  public tagIsSelected = (challengeTemplate: ChallengeTemplateViewModel, challengeTag: ChallengeTagViewModel) => {
    var index = challengeTemplate.challengeTags.findIndex(ct => ct.id == challengeTag.id);

    return index !== -1;
  };

  public removeTag = (challengeTemplate: ChallengeTemplateViewModel, challengeTag: ChallengeTagViewModel) => {
    var indexOfTag = challengeTemplate.challengeTags.indexOf(challengeTag);
    challengeTemplate.challengeTags.splice(indexOfTag, 1);
  };

  public setActiveTag = (challengeTemplate: ChallengeTemplateViewModel, challengeTag: ChallengeTagViewModel) => {
    challengeTemplate.activeChallengeTag = challengeTag;
  };

  public acceptChallenge = (template: ChallengeTemplateViewModel) => {
    // this.challengeService.add(template.id).then(() => {

    // });
  };

  public getChallengeTemplates = () => {
    this.challengeTemplateService.getAll().then(challengeTemplates => {
      this.challengeTemplateViewModels = challengeTemplates.map(challengeTemplate => {
        var viewModel = new ChallengeTemplateViewModel(challengeTemplate.value.title, challengeTemplate.value.description, challengeTemplate.key, false);
        viewModel.challengeTags = challengeTemplate.value.challengeTagIds.map(id => {
          var tagViewModel = new ChallengeTagViewModel();
          var tagRef = this.challengeTagViewModels.filter(tvm => tvm.id === id)[0];
          tagViewModel.id = id;
          tagViewModel.name = tagRef.name;
          tagViewModel.type = tagRef.type;
          return tagViewModel;
        });
        return viewModel;
      });

      this.challengeTemplateViewModels = this.challengeTemplateViewModels.sort((a, b) => {
        if (a === b) return 0;
        if (a > b) return 1;
        if (a < b) return -1;
      });

    }).catch(this.handleError);
  };

  public deleteChallengeTemplate = (challengeTemplateId: string) => {
    this.challengeTemplateService.delete(challengeTemplateId).then(() => {
      var viewModelToRemove = this.challengeTemplateViewModels.filter((viewModel) => {
        return viewModel.id === challengeTemplateId;
      })[0];

      var index = this.challengeTemplateViewModels.indexOf(viewModelToRemove);
      this.challengeTemplateViewModels.splice(index, 1);
    }).catch(this.handleError);
  }

  public updateChallengeTemplate = (templateViewModel: ChallengeTemplateViewModel) => {
    var template = new ChallengeTemplate(
      <number>templateViewModel.type,
      templateViewModel.title,
      templateViewModel.description);

    template.challengeTagIds = templateViewModel.challengeTags.map(tag => tag.id);

    templateViewModel.isSaving = true;

    this.challengeTemplateService.update(templateViewModel.id, template).then(() => {
      templateViewModel.hasChanges = false;
      templateViewModel.isSaving = false;
    }).catch(this.handleError);
  };

  public addChallengeTemplate = () => {
    var newChallengeTemplate = new ChallengeTemplate();
    newChallengeTemplate.challengeOwner = new ChallengeTemplateOwner(ChallengeTemplateOwnerType.Admin);
    newChallengeTemplate.type = ChallengeTemplateType.Standard;

    this.challengeTemplateService.add(newChallengeTemplate).then(result => {
      var viewModel = new ChallengeTemplateViewModel(result.value.title, result.value.description, result.key, true, <number>newChallengeTemplate.type);
      this.challengeTemplateViewModels.splice(0, 0, viewModel);
    }).catch(this.handleError);
  };
}