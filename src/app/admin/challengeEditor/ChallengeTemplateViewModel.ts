import { ChallengeTagViewModel } from './ChallengeTagViewModel';

export class ChallengeTemplateViewModel {

  public id: string;
  public type: ChallengeTemplateTypeViewModel;
  public title: string;
  public description: string;
  public editMode: boolean;
  public hasChanges: boolean;
  public isSaving: boolean;
  public challengeTags: ChallengeTagViewModel[];
  public activeChallengeTag: ChallengeTagViewModel;

  constructor(
    title: string = "",
    description: string = "",
    id: string = "",
    editMode: boolean = true,
    type: ChallengeTemplateTypeViewModel = ChallengeTemplateTypeViewModel.Standard) {

    this.title = title;
    this.description = description;
    this.id = id;
    this.editMode = editMode;
    this.hasChanges = false;
    this.isSaving = false;
    this.challengeTags = [];
    this.activeChallengeTag = null;
    this.type = type;
  }
}

export enum ChallengeTemplateTypeViewModel {
  Standard = 0,
  Time = 1,
  Palette = 2,
}