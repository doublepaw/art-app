import { ChallengeTag } from 'art-app-common';
import { ChallengeTagType } from 'art-app-common';
import { inject } from 'aurelia-framework';
import { ChallengeTagService } from 'common/ChallengeTagService';
import { EnumExtensions } from 'helpers/EnumExtensions';
import {Component} from '../../common/Component';

@inject(ChallengeTagService)
export class TagEditor extends Component {

  public tagViewModels: ChallengeTagViewModel[];
  private challengeTagService: ChallengeTagService;
  private challengeTagTypes: {name: string, value: number}[];

  constructor(challengeTagService: ChallengeTagService) {
    super('TagEditor');
    
    this.challengeTagService = challengeTagService;
    this.tagViewModels = [];
    this.challengeTagTypes = EnumExtensions.getNamesAndValues(ChallengeTagType);

    this.getTags();
  }

  public addTag = () => {
    var newChallengeTag = new ChallengeTag();

    this.challengeTagService.add(newChallengeTag).then(challengeTag => {
      var viewModel = new ChallengeTagViewModel();
      viewModel.id = challengeTag.key;
      viewModel.name = challengeTag.value.name;
      viewModel.type = challengeTag.value.type;

      this.tagViewModels.push(viewModel);
    }).catch(this.handleError);
  };

  public deleteTag = (tagViewModel: ChallengeTagViewModel) => {
    this.challengeTagService.delete(tagViewModel.id).then(tagRef => {
      var viewModelToRemove = this.tagViewModels.filter(viewModel => {
        return viewModel.id === tagViewModel.id;
      })[0];

      var index = this.tagViewModels.indexOf(viewModelToRemove);
      this.tagViewModels.splice(index, 1);
    }).catch(this.handleError);
  };

  public updateTag = (tagViewModel: ChallengeTagViewModel) => {
    var id = tagViewModel.id;
    var tag = new ChallengeTag(tagViewModel.name, tagViewModel.type);

    this.challengeTagService.update(tagViewModel.id, tag).then(tagRef => {
      console.log("Updated tag!");
    }).catch(this.handleError);
  };

  public getTags = () => {
    this.challengeTagService.getAll().then(challengeTags => {
      this.tagViewModels = challengeTags.map(challengeTag => {
        var viewModel = new ChallengeTagViewModel();
        viewModel.id = challengeTag.key;
        viewModel.name = challengeTag.value.name;
        viewModel.type = challengeTag.value.type;

        return viewModel;
      });
    }).catch(this.handleError);
  };
}

class ChallengeTagViewModel {
  public id: string;
  public name: string;
  public type: ChallengeTagType;
}