import { AuthService } from '../common/AuthService';
import { inject } from 'aurelia-framework';
import * as firebase from 'firebase';

@inject(AuthService)
export class TaskPushService {
  private auth: firebase.auth.Auth;
  private user: firebase.User = null;
  private authPromise: Promise<firebase.User>;
  private static taskRefString: string = 'queue/tasks';
  private tasksRef: firebase.database.Reference;

  constructor(private authService: AuthService) {
    this.authPromise = authService.getUser();
    this.tasksRef = firebase.database().ref(TaskPushService.taskRefString);
  }

  public pushTask = (taskData: any) => {
    return this.authPromise.then(user => {
      var newTaskRef = this.tasksRef.push();

      return newTaskRef.set(taskData).then(() => {
        return taskData;
      }).catch(error => {
        console.log('Failed to push task: ', error);
      });
    });
  };
}