import { ChallengeTemplate, ChallengeTemplateType, KeyValue, ISnapShot } from 'art-app-common';
import { ChallengeTemplateOwner, ChallengeTemplateOwnerType } from 'art-app-common';
import { inject } from 'aurelia-framework';
import * as firebase from 'firebase';

export class ChallengeTemplateService {
  private static baseRef: string = "challengeTemplates";
  private ref: firebase.database.Reference;

  constructor() {
    var databaseRef = firebase.database().ref();
    this.ref = databaseRef.child(ChallengeTemplateService.baseRef);
  }

  public add = (challengeTemplate: ChallengeTemplate) => {
    return new Promise<KeyValue<ChallengeTemplate>>((resolve, reject) => {
      if (challengeTemplate) {
        var newRef = this.ref.push();
        newRef.set(challengeTemplate).then(() => {

          var newChallengeTemplate = new ChallengeTemplate(
            challengeTemplate.type,
            challengeTemplate.title,
            challengeTemplate.description);

          return resolve(new KeyValue(newRef.key, newChallengeTemplate));
        }).catch(error => {
          return reject(error);
        });
      }
    });
  }

  public update = (challengeTemplateId: string, challengeTemplate: ChallengeTemplate) => {
    return new Promise<KeyValue<ChallengeTemplate>>((resolve, reject) => {
      if (challengeTemplateId && challengeTemplateId.length > 0) {
        var templateRef = this.ref.child(challengeTemplateId);
        templateRef.set(challengeTemplate).then(() => {
          resolve(new KeyValue(challengeTemplateId, challengeTemplate));
        }).catch(error => {
          reject(error);
        });
      } else {
        reject('Invalid id. Too short, null or undefined.');
      }
    });
  };


  public delete = (challengeTemplateId: string) => {
    return new Promise<void>((resolve, reject) => {
      if (challengeTemplateId && challengeTemplateId.length > 0) {
        var templateRef = this.ref.child(challengeTemplateId);
        templateRef.set(null).then(() => {
          resolve()
        }).catch(error => {
          reject(error);
        });
      } else {
        reject(new Error('Invalid id. Too short, null or undefined'));
      }
    });
  };

  public get = (challengeTemplateId: string) => {
    return new Promise<KeyValue<ChallengeTemplate>>((resolve, reject) => {
      this.ref.child(challengeTemplateId).once('value', (snapShot, b) => {
        var challengeTemplate = new ChallengeTemplate();

        challengeTemplate.title = snapShot.val().title || null;
        challengeTemplate.description = snapShot.val().description || null;
        challengeTemplate.type = snapShot.val().type || null;
        challengeTemplate.challengeTagIds = snapShot.val().challengeTagIds || null
        challengeTemplate.challengeOwner = new ChallengeTemplateOwner(ChallengeTemplateOwnerType.Admin, null);

        resolve(new KeyValue(snapShot.key, challengeTemplate));
      }).catch(error => {
        reject(error);
      });
    });
  };

  public getAll = () => {
    return new Promise<KeyValue<ChallengeTemplate>[]>((resolve, reject) => {
      this.ref.once('value', (snapShots, b) => {
        var challengeTemplates: KeyValue<ChallengeTemplate>[] = [];

        snapShots.forEach(snapShot => {
          var challengeTemplate = new ChallengeTemplate(
            <ChallengeTemplateType>snapShot.val().type,
            <string>snapShot.val().title,
            <string>snapShot.val().description);
          challengeTemplate.challengeTagIds = snapShot.val().challengeTagIds || [];
          challengeTemplate.challengeOwner = new ChallengeTemplateOwner(ChallengeTemplateOwnerType.Admin, null);

          challengeTemplates.push(new KeyValue(snapShot.key, challengeTemplate));
          return false;
        });
        
        resolve(challengeTemplates);
      }).catch(error => {
        reject(error);
      });
    });
  }
}