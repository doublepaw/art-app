import { ChallengeSuggestion } from '../../../../common/models/ChallengeSuggestion';
import { Service } from './Service'
import { inject } from 'aurelia-framework';
import { AuthService } from 'common/AuthService';
import { ChallengeTemplateService } from 'common/ChallengeTemplateService';
import * as firebase from 'firebase';
import { Challenge, ChallengeState, ChallengeTemplate, KeyValue, ISnapShot } from 'art-app-common';

@inject(AuthService, ChallengeTemplateService)
export class ChallengeService extends Service {
  private activeChallengesRef: firebase.database.Reference;
  private completedChallengesRef: firebase.database.Reference;
  private suggestedChallengesRef: firebase.database.Reference;
  private userId: string;
  private authPromise: Promise<firebase.User>;

  constructor(
    private authService: AuthService,
    private challengeTemplateService: ChallengeTemplateService) {
    super('AuthService');

    var databaseRef = firebase.database().ref();
    this.authPromise = authService.getUser();
    this.authPromise.then(user => {
      this.activeChallengesRef = databaseRef.child('activeChallenges').child(user.uid);
      this.completedChallengesRef = databaseRef.child('completedChallenges').child(user.uid);
      this.suggestedChallengesRef = databaseRef.child('suggestedChallenges').child(user.uid);
    });
  }

  public setActiveChallengesCallback = (event: string, callback: (challenge: KeyValue<Challenge>) => void) => {
    this.authPromise.then(user => {
      this.activeChallengesRef.on(event, (snapShot: ISnapShot<Challenge>) => {
        callback(this.mapSnapshotToChallenge(snapShot));
      });
    });
  };

  public setCompletedChallengesCallback = (event: string, callback: (challenge: KeyValue<Challenge>) => void) => {
    this.authPromise.then(user => {
      this.completedChallengesRef.on(event, (snapShot) => {
        callback(this.mapSnapshotToChallenge(snapShot));
      });
    });
  };

  public setSuggestedChallengesCallback = (event: string, callback: (challenge: KeyValue<ChallengeSuggestion>) => void) => {
    this.authPromise.then(user => {
      this.suggestedChallengesRef.on(event, (snapShot: ISnapShot<ChallengeSuggestion>) => {
        callback(this.mapSnapshotToSuggestion(snapShot));
      });
    });
  };

  private mapSnapshotToChallenge = (snapShot: ISnapShot<Challenge>) => {
    var challenge = new Challenge();

    challenge.title = snapShot.val().title;
    challenge.description = snapShot.val().description;
    challenge.state = snapShot.val().state;
    challenge.challengeTemplateId = snapShot.val().challengeTemplateId;
    challenge.challengeTagIds = snapShot.val().challengeTagIds;
    challenge.imageUrl = snapShot.val().imageUrl;
    challenge.completedDate = snapShot.val().completedDate;

    return new KeyValue(snapShot.key, challenge);
  };

  private mapSnapshotToSuggestion = (snapShot: ISnapShot<ChallengeSuggestion>) => {
    var suggestion = new ChallengeSuggestion();

    suggestion.title = snapShot.val().title;
    suggestion.description = snapShot.val().description;
    suggestion.challengeTemplateId = snapShot.val().challengeTemplateId;
    suggestion.challengeTagIds = snapShot.val().challengeTagIds;

    return new KeyValue(snapShot.key, suggestion);
  };

  // public add = (challengeTemplateId: string) => {
  //   return new Promise<void>((resolve, reject) => {
  //     this.authPromise.then(() => {
  //       return this.challengeTemplateService.get(challengeTemplateId).then(challengeTemplateRef => {
  //         var newChallengeRef = this.activeChallengesRef.push();
  //         var newChallenge = new ChallengeRef();

  //         newChallenge.challengeTemplateId = challengeTemplateRef.id;
  //         newChallenge.title = challengeTemplateRef.title;
  //         newChallenge.description = challengeTemplateRef.description;
  //         newChallenge.imageUrl = "";
  //         newChallenge.tagIds = challengeTemplateRef.challengeTagIds;
  //         newChallenge.state = ChallengeStateRef.Active;

  //         newChallengeRef.set(newChallenge).then(() => {
  //           return resolve();
  //         }, () => reject());
  //       }, () => reject());
  //     }, () => reject())
  //   });

  // };
}

export class ChallengeServiceEvent {
  public static Added: string = 'child_added';
  public static Removed: string = 'child_removed';
}