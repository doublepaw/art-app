import { ChallengeTag, KeyValue, ISnapShot } from 'art-app-common';
import { inject } from 'aurelia-framework';
import * as firebase from 'firebase';

export class ChallengeTagService {

  private static baseRef: string = 'challengeTags';

  private challengeTagRef: firebase.database.Reference;

  constructor() {
    var databaseRef = firebase.database().ref();
    this.challengeTagRef = databaseRef.child(ChallengeTagService.baseRef);
  }

  public getAll = () => {
    return new Promise<KeyValue<ChallengeTag>[]>((resolve, reject) => {
      this.challengeTagRef.once('value', (challengeTagSnapShots) => {
        var challengeTags: KeyValue<ChallengeTag>[] = [];

        challengeTagSnapShots.forEach((snapShot) => {
          var challengeTag = new ChallengeTag(snapShot.val().name, snapShot.val().type);
          challengeTags.push(new KeyValue(snapShot.key, challengeTag));
          return false;
        });

        return resolve(challengeTags);
      }, error => {
        console.log('Error :D ', error);
        return reject(error);
      });
    });
  };

  public delete = (challengeTagId: string) => {
    return new Promise<void>((resolve, reject) => {
      if (challengeTagId && challengeTagId.length > 0) {
        var tagRef = this.challengeTagRef.child(challengeTagId);
        return tagRef.set(null).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        });
      } else {
        reject(new Error('Invalid id. Too short, null or undefined.'));
      }
    });
  };

  public update = (challengeTagId: string, challengeTag: ChallengeTag) => {
    return new Promise<KeyValue<ChallengeTag>>((resolve, reject) => {
      if (challengeTagId && challengeTagId.length > 0) {
        var challengeTagRef = this.challengeTagRef.child(challengeTagId);

        challengeTagRef.set(challengeTag).then(() => {
          return resolve(new KeyValue(challengeTagId, challengeTag));
        }).catch(error => {
          reject(error)
        });
      } else {
        reject(new Error('Challenge tag id invalid. Too short, null or undefined.'));
      }
    });
  };

  public add = (challengeTag: ChallengeTag) => {
    return new Promise<KeyValue<ChallengeTag>>((resolve, reject) => {
      var newChallengeTagRef = this.challengeTagRef.push();

      newChallengeTagRef.set(challengeTag).then(() => {
        resolve(new KeyValue(newChallengeTagRef.key, challengeTag));
      }).catch(error => {
        reject(error);
      });
    });
  }
}