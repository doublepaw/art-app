import * as rollbar from 'rollbar-browser';
import { Logger } from 'aurelia-logging';
import * as _ from 'lodash';


// Rollbar(rollbarConfig);

export class LogAppender {

    private Rollbar: any;

    constructor() {
        var rollbarConfig = {
            accessToken: "b9b38880582249f2bfdd4879a1fe9bd7",
            captureUncaught: true,
            payload: {
                environment: "test"
            }
        };

        this.Rollbar = rollbar.init(rollbarConfig);

        console.log('Rollbar: ', this.Rollbar);
    }

    debug(logger: Logger, ...rest: any[]): void { }
    info(logger: Logger, ...rest: any[]): void { }
    warn(logger: Logger, ...rest: any[]): void {
        var warning = _.find(rest, x => x instanceof Error);
        this.Rollbar.warning(warning);
        // Display a friendly message to the user.
    }

    error = (logger: Logger, ...rest: any[]): void => {
        var error = _.find(rest, x => x instanceof Error);
        this.Rollbar.error(error);
        // Display a friendly message to the user.
    }
}