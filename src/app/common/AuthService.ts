import { FirebaseServiceFactory } from 'firebase-admin/lib/firebase-service';
import * as firebase from 'firebase';

export class AuthService {
  private auth: firebase.auth.Auth;
  private user: firebase.User = null;

  constructor() {
    this.auth = firebase.auth();
  }

  public getUser = (resolveCallback?: (user: firebase.User) => any, rejectCallback?: (error: firebase.FirebaseError) => void) => {
    return new Promise<firebase.User>((resolve, reject) => {
      this.auth.onAuthStateChanged((user: firebase.User) => {
        if (resolveCallback)
          resolveCallback(user);
        resolve(user);
      }, (error => {
        if (rejectCallback)
          rejectCallback(error as firebase.FirebaseError);
        return reject(error);
      }));
    });
  }
}