
import { RouteConfig } from 'aurelia-router';

export class HideFromNavValueConverter {
  public toView(array: RouteConfig[], count: number) {
    var filteredArray = array.filter((value) => {
      return !value.settings.hideFromNav;
    });

    return filteredArray;
  }
}