import { LogManager } from 'aurelia-framework';

export class Component {
    protected componentName;

    constructor(name: string = 'ArtApp Component') {
        this.componentName = name;

    }

    private getLogger = () => {
        return LogManager.getLogger(this.componentName);
    }

    protected handleError = (error: Error) => {
        this.getLogger().error(error);
    };
}