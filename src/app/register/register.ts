import {Component} from '../common/Component';
import * as firebase from 'firebase';
import { AppRouter } from 'aurelia-router';
import { inject } from 'aurelia-framework';

@inject(AppRouter)
export class RegisterController extends Component {

  private auth: firebase.auth.Auth;
  private email: string;
  private password: string;

  constructor(private appRouter: AppRouter) {
    super('RegisterController');

    this.auth = firebase.auth();
}

  public registerNewUser(email: string, password: string) {
    return this.auth.createUserWithEmailAndPassword(this.email, this.password).then((user: firebase.User) => {
      console.log("Successfully created ", user.uid);
      return this.appRouter.loadUrl('/profile');
    }).catch(this.handleError);
  }
}
