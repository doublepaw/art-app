import { Component } from '../common/Component';


export class HomeController extends Component {

  private title: string;

  constructor($scope: any) {
    super('HomeController');
    
    this.title = 'Home Page';
  }
}
