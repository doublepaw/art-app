import 'jquery';
import { Aurelia, Container, LogManager } from 'aurelia-framework';
import { ConsoleAppender } from 'aurelia-logging-console';
import { AuthService } from './app/common/AuthService';
import { ChallengeTemplateService } from './app/common/ChallengeTemplateService';
import {LogAppender} from '../src/app/common/LogAppender';
import * as firebase from 'firebase';
import * as bluebird from 'bluebird';


// we want font-awesome to load as soon as possible to show the fa-spinner
import './styles/styles.scss';



// comment out if you don't want a Promise polyfill (remove also from webpack.config.js)
// import * as Bluebird from 'bluebird';
// Bluebird.config({ warnings: false });

LogManager.addAppender(new ConsoleAppender());
LogManager.addAppender(new LogAppender());
LogManager.setLevel(LogManager.logLevel.debug);

export async function configure(aurelia: Aurelia, container: Container) {
  var globalResources = [
    'app/common/HideFromNavValueConverter'
  ];


  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    // .plugin('aurelia-animator-css')
    .globalResources(globalResources);

  aurelia.container.registerSingleton(AuthService);
  // aurelia.container.registerSingleton(ChallengeTemplateService);

  // Uncomment the line below to enable animation.
  // aurelia.use.plugin('aurelia-animator-css');
  // if the css animator is enabled, add swap-order="after" to all router-view elements

  // Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  // aurelia.use.plugin('aurelia-html-import-template-loader')

  await aurelia.start();
  aurelia.setRoot('app/app');

  // if you would like your website to work offline (Service Worker), 
  // install and enable the @easy-webpack/config-offline package in webpack.config.js and uncomment the following code:
  /*
  const offline = await System.import('offline-plugin/runtime');
  offline.install();
  */

  var config = {
    apiKey: "AIzaSyCbJMHYbnEIZd2_sUw9T3KTtmqZ_vHYyFc",
    authDomain: "art-app-22a6c.firebaseapp.com",
    databaseURL: "https://art-app-22a6c.firebaseio.com",
    storageBucket: "gs://art-app-22a6c.appspot.com",
    messagingSenderId: "1084391649617"
  };

  firebase.initializeApp(config);

  bluebird.config({ warnings: false });
}