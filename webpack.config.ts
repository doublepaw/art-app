import * as easyWebpack from '@easy-webpack/core';
import * as webpack from 'webpack';

import { generateConfig, get, stripMetadata, EasyWebpackConfig, WebpackConfigWithMetadata } from '@easy-webpack/core';
import * as path from 'path';

import * as typescript from '@easy-webpack/config-typescript';
import * as sass from '@easy-webpack/config-sass';
import * as aurelia from '@easy-webpack/config-aurelia';
import * as envDev from '@easy-webpack/config-env-development';
import * as envProd from '@easy-webpack/config-env-production';
import * as html from '@easy-webpack/config-html';
import * as fontsAndImages from '@easy-webpack/config-fonts-and-images';
import * as globalBluebird from '@easy-webpack/config-global-bluebird';
import * as globalJquery from '@easy-webpack/config-global-jquery';
import * as generateIndexHtml from '@easy-webpack/config-generate-index-html';
import * as copyFiles from '@easy-webpack/config-copy-files';
import * as optimizeCommonChunks from '@easy-webpack/config-common-chunks-simple';

const ENV:
  'development'
  | 'production'
  | 'test' = process.env.NODE_ENV
  && process.env.NODE_ENV.toLowerCase()
  || (process.env.NODE_ENV = 'development');


console.log("Running in ", ENV, " mode.");

const title = 'ArtApp Configuration';
const baseUrl = '/';
const rootDir = path.resolve('');
const srcDir = path.resolve('src');
const outDir = path.resolve('dist');
const outDirServ = path.resolve('serveDist');

console.log("rootDir ", rootDir);
console.log("srcDir ", srcDir);
console.log("outDir ", outDir);

var bundles = {
  firebase: [
    'firebase'
  ],
  bootstrapper: [
    'aurelia-bootstrapper-webpack',
    'aurelia-polyfills',
    'aurelia-pal',
    'aurelia-pal-browser',
    'regenerator-runtime',
    'jquery'
  ],
  aurelia: [
    'aurelia-framework',
    // 'aurelia-bootstrapper-webpack',
    'aurelia-binding',
    'aurelia-dependency-injection',
    'aurelia-event-aggregator',
    'aurelia-fetch-client',
    'aurelia-history',
    'aurelia-history-browser',
    'aurelia-loader',
    'aurelia-loader-webpack',
    'aurelia-logging',
    'aurelia-logging-console',
    'aurelia-metadata',
    // 'aurelia-pal',
    // 'aurelia-pal-browser',
    'aurelia-path',
    // 'aurelia-polyfills',
    'aurelia-route-recognizer',
    'aurelia-router',
    'aurelia-task-queue',
    'aurelia-templating',
    'aurelia-templating-binding',
    'aurelia-templating-router',
    'aurelia-templating-resources',
    // 'aurelia-animator-css'
  ]
}

const common: EasyWebpackConfig = {
  entry: {
    'app': ['./src/main'],
    // 'vendor': bundles['vendor'],
    // 'foundation-sites': 'foundation-sites!./config/foundation-sites.config.js',
    'aurelia-bootstrap': bundles['bootstrapper'],
    'aurelia': bundles['aurelia'].filter(pkg => bundles.bootstrapper.indexOf(pkg) === -1),
    'firebase': bundles['firebase']
  },
  output: {
    path: outDir,
    publicPath: ''
  },
  // devServer: {
  //   contentBase: 'dist'
  // },
  plugins: [
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery',
      'window.jQuery': 'jquery',
      'window.$': 'jquery',
    })
  ],
  resolve: {
    modules: ["node_modules", "src/app", "src/helpers", "src/app/common", "src"],
    extensions: ['.ts', '.js', '.html', '.njk', '.css', '.scss', '.less', '.json', '.yml'],
    alias: {
      'jquery': 'jquery/src/jquery',
    }
  },
}
// { devtool: 'cheap-module-inline-source-map' }
var development: EasyWebpackConfig = envDev();
// { devtool: 'source-map' }
var production: EasyWebpackConfig = envProd({
  loaderOptions: {
    sassLoader: {
      includePaths: [path.resolve(srcDir, 'styles')]
    },
    resolveUrlLoader: {
      // Stuff goes in here for resolve-url-loader
    },
    output: {
      path: outDir
    },
    context: srcDir
  }
});

// <WebpackConfigWithMetadata>production.

// The first source is 'stdin' according to libsass because we've used the data input
// Now let's override that value with the correct relative path
// result.map.sources[0] = path.relative(self.options.context, resourcePath);
// result.map.sourceRoot = path.relative(self.options.context, process.cwd());

console.log('index.html: ', path.join(srcDir, 'index.html'));
const aureliaConfig: EasyWebpackConfig = aurelia({ root: rootDir, src: srcDir, title: title, baseUrl: baseUrl });
const htmlConfig: EasyWebpackConfig = html({ exclude: './src/index.html' });
const sassConfig: EasyWebpackConfig = sass({ filename: 'styles.css', resolveRelativeUrl: true, allChunks: true, sourceMap: true, });
// const sassConfig: EasyWebpackConfig = {

// };
const typescriptConfig: EasyWebpackConfig = typescript();
const fontsAndImagesConfig: EasyWebpackConfig = fontsAndImages();
const globalBluebirdConfig: EasyWebpackConfig = globalBluebird();
const globalJqueryConfig: EasyWebpackConfig = globalJquery();
const generateIndexHtmlConfig: EasyWebpackConfig = generateIndexHtml({
  minify: false,
  overrideOptions: {
    template: path.join('index.html')
    // filename: path.join(outDir, 'index.html')
  }
});
const copyFilesConfig: EasyWebpackConfig = copyFiles({
  patterns: [{
    from: path.join(srcDir, 'index.html'),
    to: path.join(outDir, 'index.html')
  }]
});

const optimizeCommonChunksConfig: EasyWebpackConfig = optimizeCommonChunks({
  appChunkName: 'app', firstChunk: 'aurelia-bootstrap'
})

let config: EasyWebpackConfig = {};

switch (process.env.NODE_ENV) {
  case 'development':
    config = generateConfig(
      common,
      development,
      aureliaConfig,
      htmlConfig,
      typescriptConfig,
      sassConfig,
      fontsAndImagesConfig,
      globalBluebirdConfig,
      globalJqueryConfig,
      copyFilesConfig,
      generateIndexHtmlConfig,
      optimizeCommonChunksConfig);
    break;
  case 'production':
    config = generateConfig(
      common,
      production,
      aureliaConfig,
      htmlConfig,
      typescriptConfig,
      sassConfig,
      fontsAndImagesConfig,
      globalBluebirdConfig,
      globalJqueryConfig,
      copyFilesConfig,
      generateIndexHtmlConfig,
      optimizeCommonChunksConfig);
    break;
}

module.exports = stripMetadata(config);